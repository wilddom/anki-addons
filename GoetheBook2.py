import itertools, os, collections, sqlite3, AddOnLib, AddOnLib.AnkiHelper

from aqt import mw
from aqt.qt import *
from anki.media import MediaManager
from PyQt4.QtGui import QDialog

class SelectTypesDialog(QDialog):
    def __init__(self):
        super(SelectTypesDialog, self).__init__()
        
        self.setWindowTitle("Choose what you want to import")
        layout = QVBoxLayout(self)
        
        self.importLessons = QCheckBox("Lessons")
        self.importLessons.setChecked(True)
        layout.addWidget(self.importLessons)
        
        self.importVocabulary = QCheckBox("Vocabulary")
        self.importVocabulary.setChecked(True)
        layout.addWidget(self.importVocabulary)
        
        self.importOpposites = QCheckBox("Opposites")
        self.importOpposites.setChecked(True)
        layout.addWidget(self.importOpposites)
        
        buttons = QDialogButtonBox(QDialogButtonBox.Ok, Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        layout.addWidget(buttons)

class SelectLanguageDialog(QDialog):
    def __init__(self, title="Select language"):
        super(SelectLanguageDialog, self).__init__()
        
        self.setWindowTitle(title)
        layout = QVBoxLayout(self)
                
        self.languageSelection = QComboBox()
        layout.addWidget(self.languageSelection)
        
        buttons = QDialogButtonBox(QDialogButtonBox.Ok, Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        layout.addWidget(buttons)

    def reset(self):
        self.languageSelection.clear()

    def addLanguage(self, language, data=""):
        self.languageSelection.addItem(language, data)
        
    def getSelectedLanguage(self):
        return self.languageSelection.currentText()
        
    def getSelectedData(self):
        return self.languageSelection.itemData(self.languageSelection.currentIndex())


class ImporterException(Exception):
    pass

class Importer(object):
    def __init__(self, col):
        self.col = col
        self.sourceLanguage = None
        self.targetLanguage = None
        
        self.path = ""
        self.audioSrcPath = ""
        
        self.connection = None
        
        self.anki = AddOnLib.AnkiHelper.AnkiHelper(self.col)

    @staticmethod
    def findSubdirectory(path):
        for currdir, dirs, _ in AddOnLib.walk(path, 5):
            if "fiftylanguages" in dirs and "p50languages" in dirs:
                return currdir
        return ""

    def hasLessons(self):
        cursor = self.connection.cursor()
        cursor.execute("select count(*) as number from tblLesson where languageId=?", (self.targetLanguage['languageId'],))
        row = cursor.fetchone()
        if row and row["number"] > 0:
            cursor.execute("select count(*) as number from tblLesson where languageId=?", (self.sourceLanguage['languageId'],))
            row = cursor.fetchone()
            if row and row["number"] > 0:
                return True
        return False

    def getMainDeckName(self):
        return u'50languages {}-{}'.format(self.sourceLanguage['languageCode'], self.targetLanguage['languageCode'])

    def importLessons(self):
        model = self.anki.createModel(u'::'.join([self.getMainDeckName(), 'Lessons']), [self.sourceLanguage['languageName'], u'{} {}'.format(self.sourceLanguage['languageName'], 'Transcription'), self.targetLanguage['languageName'], u'{} {}'.format(self.targetLanguage['languageName'], 'Transcription'), 'Audio', 'Level'])
        self.anki.addDefaultAudioFieldDecorator('Audio')
        self.anki.addTemplate(model, self.anki.createTemplate(u'{} -> {}'.format(self.sourceLanguage['languageName'], self.targetLanguage['languageName']), [self.sourceLanguage['languageName']], [self.targetLanguage['languageName'], 'Audio']))
        self.anki.selectModel(model)
        deck = self.anki.createDeck(u'::'.join([self.getMainDeckName(), 'Lessons']))
        self.anki.selectDeck(deck)
        self.anki.saveDeckModelRelation(deck, model)
        
        audioId = 0
        
        cursor1 = self.connection.cursor()
        cursor2 = self.connection.cursor()
        for lesson in cursor1.execute("select tgt.*, src.lessonId as sourceLessonId, src.lessonName as sourceLessonName from tblLesson as tgt join tblLesson as src on tgt.sortNo=src.sortNo where tgt.languageId=? and src.languageId=? order by sortNo", (self.targetLanguage['languageId'], self.sourceLanguage['languageId'])):
            tags = [self.anki.prepareLevelTag(lesson["sortNo"], 2)]
            titleTag = self.anki.prepareTitleTag(lesson["sourceLessonName"])
            if titleTag:
                tags.append(titleTag)
                
            for term in cursor2.execute("select tgt.*, src.value as sourceValue from tblLessonData as tgt join tblLessonData as src on tgt.key=src.key where tgt.lessonId=? and src.lessonId=? order by cast(tgt.key as integer)", (lesson['lessonId'], lesson['sourceLessonId'])):
                note = self.col.newNote()
                
                sourceValues = term["sourceValue"].split("@")
                note[self.sourceLanguage['languageName']] = self.anki.prepareText(sourceValues[0])
                if len(sourceValues) > 1:
                    note[u'{} {}'.format(self.sourceLanguage['languageName'], 'Transcription')] = self.anki.prepareText(sourceValues[1])
                
                targetValues = term["value"].split("@")
                note[self.targetLanguage['languageName']] = self.anki.prepareText(targetValues[0])
                if len(targetValues) > 1:
                    note[u'{} {}'.format(self.targetLanguage['languageName'], 'Transcription')] = self.anki.prepareText(targetValues[1])
                
                note["Level"] = str(lesson["sortNo"])
                
                audioId += 1
                audioSrcFile = os.path.join(self.audioSrcPath, "{}-B{:04}.mp3".format(self.targetLanguage['languageCode'], int(audioId)))
                if os.path.isfile(audioSrcFile):
                    audioDstFile = self.anki.copyToMediaDir(audioSrcFile)
                    note["Audio"] = self.anki.prepareAudio(os.path.basename(audioDstFile))

                for tag in tags:
                    note.addTag(tag)

                self.col.addNote(note)
    
    def hasVocabulary(self):
        cursor = self.connection.cursor()
        cursor.execute("select count(*) as number from tblWords where lan_code=?", (self.targetLanguage['languageCode'],))
        row = cursor.fetchone()
        if row and row["number"] > 0:
            cursor.execute("select count(*) as number from tblWords where lan_code=?", (self.sourceLanguage['languageCode'],))
            row = cursor.fetchone()
            if row and row["number"] > 0:
                return True
        return False
    
    def importVocabulary(self):
        imagePath = os.path.join(self.path, 'fiftylanguages', 'VOCAB')
        if not os.path.isdir(imagePath):
            raise ImporterException("Image path not found: '{}'!".format(imagePath))
        
        model = self.anki.createModel(u'::'.join([self.getMainDeckName(), 'Vocabulary']), [self.sourceLanguage['languageName'], u'{} {}'.format(self.sourceLanguage['languageName'], 'Transcription'), self.targetLanguage['languageName'], u'{} {}'.format(self.targetLanguage['languageName'], 'Transcription'), 'Audio', 'Image', 'Level'])
        self.anki.addDefaultAudioFieldDecorator('Audio')
        self.anki.addDefaultImageFieldDecorator('Image')
        self.anki.addTemplate(model, self.anki.createTemplate(u"{} -> {}".format(self.sourceLanguage['languageName'], self.targetLanguage['languageName']), [self.sourceLanguage['languageName'], 'Image'], [self.targetLanguage['languageName'], 'Audio']))
        self.anki.addTemplate(model, self.anki.createTemplate(u"{} -> {}".format(self.targetLanguage['languageName'], self.sourceLanguage['languageName']), [self.targetLanguage['languageName'], 'Audio'], [self.sourceLanguage['languageName'], 'Image']))
        self.anki.selectModel(model)
        deck = self.anki.createDeck(u'::'.join([self.getMainDeckName(), 'Vocabulary']))
        self.anki.selectDeck(deck)
        self.anki.saveDeckModelRelation(deck, model)
        
        cursor1 = self.connection.cursor()
        cursor2 = self.connection.cursor()
        for topic in cursor1.execute("select tgt.*, src.topic as sourceTopic, timg.image as image from tblTopics as tgt join tblTopics as src on tgt.topic_id=src.topic_id join tblTopicIds as timg on src.topic_id=timg.id where tgt.lan_code=? and src.lan_code=? order by topic_id", (self.targetLanguage['languageCode'], self.sourceLanguage['languageCode'])):
            tags = [self.anki.prepareLevelTag(topic["topic_id"], 2)]
            titleTag = self.anki.prepareTitleTag(topic["sourceTopic"])
            if titleTag:
                tags.append(titleTag)
            
            note = self.col.newNote()
            
            sourceValues = topic["sourceTopic"].split("@")
            note[self.sourceLanguage['languageName']] = self.anki.prepareText(sourceValues[0])
            if len(sourceValues) > 1:
                note[u'{} {}'.format(self.sourceLanguage['languageName'], 'Transcription')] = self.anki.prepareText(sourceValues[1])
            
            targetValues = topic["topic"].split("@")
            note[self.targetLanguage['languageName']] = self.anki.prepareText(targetValues[0])
            if len(targetValues) > 1:
                note[u'{} {}'.format(self.targetLanguage['languageName'], 'Transcription')] = self.anki.prepareText(targetValues[1])
            
            note["Level"] = str(topic["topic_id"])
            
            audioSrcFile = os.path.join(self.audioSrcPath, "{}vocab_x{:02}.mp3".format(self.targetLanguage['languageCode'], int(topic["topic_id"])))
            if os.path.isfile(audioSrcFile):
                audioDstFile = self.anki.copyToMediaDir(audioSrcFile)
                note["Audio"] = self.anki.prepareAudio(os.path.basename(audioDstFile))

            imageSrcFile = os.path.join(imagePath, 'vocab_{}'.format(topic["image"]))
            if os.path.isfile(imageSrcFile):
                imageDstFile = self.anki.copyToMediaDir(imageSrcFile)
                note["Image"] = self.anki.prepareImage(os.path.basename(imageDstFile))

            for tag in tags:
                note.addTag(tag)

            self.col.addNote(note)
            
            for term in cursor2.execute("select tgt.*, src.word as sourceWord, wrd.image as image from tblWords as tgt join tblWords as src on tgt.word_id=src.word_id join tblWordIds as wrd on tgt.word_id=wrd.id where wrd.topic_id=? and tgt.lan_code=? and src.lan_code=? order by word_id", (topic['topic_id'], self.targetLanguage['languageCode'], self.sourceLanguage['languageCode'])):
                note = self.col.newNote()
            
                sourceValues = term["sourceWord"].split("@")
                note[self.sourceLanguage['languageName']] = self.anki.prepareText(sourceValues[0])
                if len(sourceValues) > 1:
                    note[u'{} {}'.format(self.sourceLanguage['languageName'], 'Transcription')] = self.anki.prepareText(sourceValues[1])
                
                targetValues = term["word"].split("@")
                note[self.targetLanguage['languageName']] = self.anki.prepareText(targetValues[0])
                if len(targetValues) > 1:
                    note[u'{} {}'.format(self.targetLanguage['languageName'], 'Transcription')] = self.anki.prepareText(targetValues[1])
                
                note["Level"] = str(topic["topic_id"])
                
                audioSrcFile = os.path.join(self.audioSrcPath, "{}vocab_{:04}.mp3".format(self.targetLanguage['languageCode'], int(term["word_id"])))
                if os.path.isfile(audioSrcFile):
                    audioDstFile = self.anki.copyToMediaDir(audioSrcFile)
                    note["Audio"] = self.anki.prepareAudio(os.path.basename(audioDstFile))
    
                imageSrcFile = os.path.join(imagePath, 'vocab_{}'.format(term["image"]))
                if os.path.isfile(imageSrcFile):
                    imageDstFile = self.anki.copyToMediaDir(imageSrcFile)
                    note["Image"] = self.anki.prepareImage(os.path.basename(imageDstFile))
    
                for tag in tags:
                    note.addTag(tag)
    
                self.col.addNote(note) 
    
    def hasOpposites(self):
        cursor = self.connection.cursor()
        cursor.execute("select count(*) as number from tblOppositeCouple where lang_code=?", (self.targetLanguage['languageCode'],))
        row = cursor.fetchone()
        if row and row["number"] > 0:
            return True
        return False
    
    def importOpposites(self):
        model = self.anki.createModel(u'::'.join([self.getMainDeckName(), 'Opposites']), ["Word", "Opposite", "Attributes"])
        self.anki.addFieldDecorator('Attributes', lambda x: u"{{#Attributes}}<br /><span class=\"attrs\">("+x+u")</span>{{/Attributes}}")
        self.anki.addTemplate(model, self.anki.createTemplate(u"Forward", ["Word", 'Attributes'], ["Opposite"]))
        self.anki.addTemplate(model, self.anki.createTemplate(u"Reverse", ["Opposite", 'Attributes'], ["Word"]))
        self.anki.selectModel(model)
        deck = self.anki.createDeck(u'::'.join([self.getMainDeckName(), 'Opposites']))
        self.anki.selectDeck(deck)
        self.anki.saveDeckModelRelation(deck, model)
        
        cursor1 = self.connection.cursor()
        for opposite in cursor1.execute("select opp.*, pos.part_of_speech_name as part_of_speech from tblOppositeCouple as opp join tblOppositeCoupleIds as oja on opp.couple_id=oja.serial join tblPartOfSpeech as pos on oja.part_of_speech_Id=pos.serial where opp.lang_code=?", (self.targetLanguage['languageCode'], )):            
            note = self.col.newNote()

            note["Word"] = self.anki.prepareText(opposite["word1"])
            note["Opposite"] = self.anki.prepareText(opposite["word2"])
            note["Attributes"] = self.anki.prepareText(opposite["part_of_speech"])

            self.col.addNote(note) 

    def open(self, path):
        self.path = self.findSubdirectory(os.path.abspath(path))
        if not self.path:
            raise ImporterException("The chosen directory does not contain the folders 'fiftylanguages' and 'p50languages'!")
        
        dbfilename = os.path.join(self.path, 'p50languages', 'pbd123.db')
        if not os.path.isfile(dbfilename):
            raise ImporterException("Database file not found: '{}'!".format(dbfilename))
        
        self.connection = sqlite3.connect(dbfilename)
        self.connection.row_factory = sqlite3.Row
    
    def selectLanguage(self, title):
        cursor = self.connection.cursor()
        dialog = SelectLanguageDialog(title)
        for row in cursor.execute("select * from tblLanguage order by languageCode"):
            dialog.addLanguage(u"{} {}".format(row["languageCode"], row["languageName"]), row)
        dialog.exec_()
        return dialog.getSelectedData()
    
    def selectSourceLanguage(self):
        self.sourceLanguage = self.selectLanguage("Select source language")
        
    def selectTargetLanguage(self):
        self.targetLanguage = self.selectLanguage("Select target language")
        
        self.audioSrcPath = os.path.join(self.path, 'fiftylanguages', self.targetLanguage["languageCode"])
        if not os.path.isdir(self.audioSrcPath):
            raise ImporterException("Audio path not found: '{}'!".format(self.audioSrcPath))

def startImport():
    QMessageBox.information(mw, "Instructions", "Select the language in the 50 Language android app and transfer "
                                                "the folders 'fiftylanguages' and 'p50languages' to your computer.")
    d = QFileDialog(mw)
    d.setWindowTitle("Select the location of the folders")
    d.setFileMode(QFileDialog.Directory)
    d.setOption(QFileDialog.ShowDirsOnly, True)
    if d.exec_() == QDialog.Accepted:
        dirs = d.selectedFiles()
        if len(dirs):
            try:
                importer = Importer(mw.col)
                importer.open(dirs[0])
                importer.selectSourceLanguage()
                importer.selectTargetLanguage()
                
                dialog = SelectTypesDialog()
                if not importer.hasLessons():
                    dialog.importLessons.setChecked(False)
                    dialog.importLessons.setDisabled(True)
                if not importer.hasVocabulary():
                    dialog.importVocabulary.setChecked(False)
                    dialog.importVocabulary.setDisabled(True)
                if not importer.hasOpposites():
                    dialog.importOpposites.setChecked(False)
                    dialog.importOpposites.setDisabled(True)
                dialog.exec_()
                
                if dialog.importLessons.isChecked():
                    importer.importLessons()
                if dialog.importVocabulary.isChecked():
                    importer.importVocabulary()
                if dialog.importOpposites.isChecked():
                    importer.importOpposites()
            
                mw.col.reset()
                mw.reset()
                # refresh deck browser so user can see the newly imported deck
                mw.deckBrowser.refresh()
            except ImporterException as e:
                QMessageBox.warning(mw, "Error", str(e))

action = QAction("Import Book2...", mw)
mw.connect(action, SIGNAL("triggered()"), startImport)
mw.form.menuTools.addAction(action)

