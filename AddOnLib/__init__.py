import os

def walk(path, level=None):
    path = os.path.abspath(path)
    if not os.path.isdir(path):
        return
    num_sep = path.count(os.path.sep)
    for root, dirs, files in os.walk(path):
        yield root, dirs, files
        if not level is None and num_sep + level <= root.count(os.path.sep):
            return