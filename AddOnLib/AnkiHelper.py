import uuid, os, shutil
from anki.media import MediaManager

class AnkiHelper(object):
    def __init__(self, col):
        self.col = col
        self.fieldDecorators = {}
        self.mediaPath = MediaManager(self.col, None).dir()
    
    def copyToMediaDir(self, path, rename=True, prefix=""):
        if os.path.isfile(path):
            _, fileExtension = os.path.splitext(path)
            audioDstFile = os.path.join(self.mediaPath, os.path.basename(path))
            if rename:
                audioDstFile = os.path.join(self.mediaPath, (str(uuid.uuid5(uuid.NAMESPACE_URL, os.path.join(prefix, path.encode('utf-8'))))+fileExtension))                
            shutil.copy(path, audioDstFile)
            return audioDstFile
        return path
    
    @staticmethod
    def prepareTitleTag(tag):
        value = u''.join(x for x in tag.title() if x.isalnum())
        if value.isdigit():
            return ''
        return value
    
    @staticmethod
    def prepareLevelTag(levelNum, width, prefix=u"Level"):
        formatstr = u"{}{:0"+unicode(width)+"d}"
        return formatstr.format(prefix, levelNum)
    
    @staticmethod
    def prepareText(content):
        return u'{:s}'.format(content.strip())
    
    @staticmethod
    def prepareAudio(content):
        return u'[sound:{:s}]'.format(content)
    
    @staticmethod
    def prepareImage(content):
        return u'<img src="{:s}">'.format(content)
    
    def findOrCreateNote(self, value, deck=None, model=None):
        note = self.findNote(value, deck, model)
        if note is None:
            return self.col.newNote()
        return note
    
    def findNote(self, value, deck=None, model=None):
        if deck is None:
            deck = self.col.decks.current()
        if model is None:
            model = self.col.models.current()
        notes = self.col.findNotes(u'deck:"{}" "dupe:{}{}"'.format(deck['name'], model['id'], value))
        if notes:
            return self.col.getNote(notes[0])
        return None
    
    def createModel(self, title, fields=[]):
        mm = self.col.models
                
        name = u"{}".format(title)
        
        if mm.byName(name):
            name += u"-{}".format(uuid.uuid4())
        
        m = mm.new(name)
        
        for field in fields:
            fm = mm.newField(field)
            mm.addField(m, fm)
        
        m['css'] += "\n.alts {\n font-size: 14px;\n}"
        m['css'] += "\n.attrs {\n font-style: italic;\n font-size: 14px;\n}"
        
        mm.add(m)
        
        return m

    def addFieldDecorator(self, name, func):
        self.fieldDecorators[name] = func
    
    def addDefaultAudioFieldDecorator(self, name):
        self.fieldDecorators[name] = lambda x: u"{{#"+name+u"}}<div style=\"display:none;\">"+x+u"</div>{{/"+name+u"}}"
        
    def addDefaultImageFieldDecorator(self, name):
        self.fieldDecorators[name] = lambda x: u"{{#"+name+u"}}<br/>"+x+u"{{/"+name+u"}}"
        
    def decorateField(self, name):
        decorate = self.fieldDecorators.get(name)
        value = u"{{"+name+u"}}"
        if not decorate:
            return value
        return decorate(value)

    def createTemplate(self, name, frontFields=[], backFields=[]):
        mm = self.col.models
        
        t = mm.newTemplate(name)
        t['qfmt'] = u"<br />\n".join([self.decorateField(field) for field in frontFields])
        t['afmt'] = u"{{FrontSide}}\n\n<hr id=\"answer\" />\n\n"
        t['afmt'] += u"<br />\n".join([self.decorateField(field) for field in backFields])
        
        return t

    def addTemplate(self, model, template):
        self.col.models.addTemplate(model, template)

    def createDeck(self, name):
        did = self.col.decks.id(name, create=False)
        if did:
            did = self.col.decks.id(u"{}-{}".format(name, uuid.uuid4()))
        else:
            did = self.col.decks.id(name, create=True)
        return self.col.decks.get(did)

    def selectDeck(self, deck):
        did = self.col.decks.id(deck["name"], create=False)
        if did:
            self.col.decks.select(did)

    def selectModel(self, model):
        if model and not model['id']:
            self.col.models.add(model)
        else:
            self.col.models.save(model)
        self.col.models.setCurrent(model)

    def saveDeckModelRelation(self, deck, model):
        deck['mid'] = model['id']
        self.col.decks.save(deck)
        
        model["did"] = deck["id"]
        self.col.models.save(model)
        