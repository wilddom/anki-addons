import urllib, urllib2, itertools, urlparse, os, AddOnLib.AnkiHelper

from BeautifulSoup import BeautifulSoup
import uuid

from aqt import mw
from aqt.qt import *
from anki.media import MediaManager

def download(url, destinationDirectory):
    sourcePath = urlparse.urlparse(url).path
    contentExtension = os.path.splitext(sourcePath)[1]
    localName = "{:s}{:s}".format(uuid.uuid5(uuid.NAMESPACE_URL, url.encode('utf-8')), contentExtension)
    fullMediaPath = os.path.join(destinationDirectory, localName)
    with open(fullMediaPath, "wb") as mediaFile:
        mediaFile.write(urllib2.urlopen(url).read())
    return fullMediaPath

def getAttribute(description):
    if description == 'noun':
        return 's'
    elif description == 'adjective':
        return 'adj'
    elif description == 'verb':
        return 'v'

def getCase(description):
    if description == '(ja)':
        return '1./sg'
    elif description == '(ty)':
        return '2./sg'
    elif description == '(on,ona)':
        return '3./sg'
    elif description == '(my)':
        return '1./pl'
    elif description == '(vy)':
        return '2./pl'
    elif description == '(oni)':
        return '3./pl'
        
def importLittleCzechPrimer():
    anki = AddOnLib.AnkiHelper.AnkiHelper(mw.col)
    model = anki.createModel('Little Czech Primer', ['English', 'Czech', 'Attributes', 'Image', 'Audio'])
    anki.addFieldDecorator('Attributes', lambda x: u"{{#Attributes}}<br /><span class=\"attrs\">("+x+u")</span>{{/Attributes}}")
    anki.addFieldDecorator('Audio', lambda x: u"{{#Audio}}<div style=\"display:none;\">"+x+u"</div>{{/Audio}}")
    anki.addTemplate(model, anki.createTemplate(u"{} -> {}".format('English', 'Czech'), ['English', 'Attributes', 'Image'], ['Czech', 'Audio']))
    anki.addTemplate(model, anki.createTemplate(u"{} -> {}".format('Czech', 'English'), ['Czech', 'Attributes', 'Audio'], ['English', 'Image']))
    anki.selectModel(model)
    deck = anki.createDeck('Little Czech Primer')
    anki.selectDeck(deck)
    anki.saveDeckModelRelation(deck, model)
    
    downloadDirectory = MediaManager(mw.col, None).dir()
    
    baseurl = 'http://czechprimer.org/'
    indexPage = urllib2.urlopen(urlparse.urljoin(baseurl, urllib.quote('/contents/en'))).read()
    indexSoup = BeautifulSoup(indexPage)
    items = list(itertools.chain.from_iterable([ul.findAll('li') for ul in indexSoup.findAll('ul', 'contents')]))
    for idx, item in enumerate(items):
        print idx
        note = mw.col.newNote()
        
        anchor = item.find('a')
        note['Czech'] = anchor['title']
        note['English'] = anchor.string
        partOfSpeech = item.find('img')['title']
        note["Attributes"] = getAttribute(partOfSpeech)
        wordUrl = urlparse.urljoin(baseurl, urllib.quote(anchor['href']))
        wordPage = urllib2.urlopen(wordUrl).read()
        wordSoup = BeautifulSoup(wordPage)
        
        imageSoup = wordSoup.find(attrs={'id': 'picture'})
        if imageSoup:
            try:
                imageUrl = urlparse.urljoin(baseurl, urllib.quote(imageSoup['src']))
                imageFilename = download(imageUrl, downloadDirectory)
                note["Image"] = '<img src="{}" />'.format(os.path.basename(imageFilename))
            except urllib2.HTTPError as e:
                print "No image found: {}".format(imageUrl)
        
        try:
            audioUrl = urlparse.urljoin(baseurl, urllib.quote('/media/words/'+urllib.unquote(wordUrl.split('/')[-1]).replace(' ','')+'.mp3'))
            audioFilename = download(audioUrl, downloadDirectory)
            note["Audio"] = '[sound:{}]'.format(os.path.basename(audioFilename))
        except urllib2.HTTPError as e:
            print "No audio found: {}".format(audioUrl)
        
        mw.col.addNote(note)
        
        conjugate = False
        if partOfSpeech == 'verb':
            conjugate = wordSoup.find(attrs={'id': 'conjugate'})
            if conjugate:
                conjugateUrl = urlparse.urljoin(baseurl, urllib.quote(conjugate.findParent('a')['href']))
                conjugatePage = urllib2.urlopen(conjugateUrl).read()
                conjugateSoup = BeautifulSoup(conjugatePage)
                for row in conjugateSoup.find('table').findAll('tr'):
                    conjugateNote = mw.col.newNote()
                    
                    cells = row.findAll('td')
                    conjugateNote['Czech'] = cells[1].string
                    conjugateNote['English'] = cells[2].string
                    conjugateNote["Attributes"] = '/'.join([note["Attributes"], getCase(cells[0].string)])
                    conjugateNote["Image"] = note["Image"]
                    
                    mw.col.addNote(conjugateNote)
    
    print "finished"            
    
    mw.col.reset()
    mw.reset()
    
    # refresh deck browser so user can see the newly imported deck
    mw.deckBrowser.refresh()
    

action = QAction("Import Little Czech Primer...", mw)
mw.connect(action, SIGNAL("triggered()"), importLittleCzechPrimer)
mw.form.menuTools.addAction(action)

