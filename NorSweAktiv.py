import os, glob, re, uuid, shutil, AddOnLib, AddOnLib.AnkiHelper

from aqt import mw
from aqt.qt import *
from PyQt4.Qt import QDialog

class Entry(object):
    def __init__(self):
        self.word = ""
        self.definition = ""
        self.id = None
        self.nr = None
 
    def isEmpty(self):
        return not self.word and not self.definition and self.id is None and self.nr is None
 
class EntryFile(object):
    def __init__(self, fileobject):
        self.file = fileobject
     
    def fromStream(self, encoding="iso-8859-1"):
        entry = Entry()
        try:
            entry.word = unicode(self.__getNext().strip(), encoding)
            entry.definition = unicode(self.__getNext().strip(), encoding)
            entry.id = self.__toInt(self.__getNext().strip())
            entry.nr = self.__toInt(self.__getNext().strip())
            return entry
        except EOFError:
            return None
     
    def __toInt(self, value):
        try:
            return int(value)
        except ValueError:
            return None
     
    def __getNext(self):
        for line in self.file:
            return line
        raise EOFError()
     
    def __iter__(self):
        entry = self.fromStream()
        while entry:
            if not entry.isEmpty():
                yield entry
            entry = self.fromStream()

class ImporterException(Exception):
    pass

class Importer(object):

    def __init__(self, col):
        self.col = col
        self.path = ""
        self.audioSrcPath = ""
        self.language = ""
        self.anki = AddOnLib.AnkiHelper.AnkiHelper(self.col)

    @staticmethod
    def findSubdirectory(path):
        for currdir, dirs, _ in AddOnLib.walk(path, 5):
            if "lektionen" in dirs and "lekvok" in dirs:
                return currdir
        return ""

    @staticmethod
    def findLanguage(path):
        if os.path.isfile(os.path.join(path, 'n_aktiv.exe')):
            return 'Norwegisch'
        elif os.path.isfile(os.path.join(path, 's_aktiv.exe')):
            return 'Schwedisch'
        return ""

    def importFile(self, lessonFilename, level):
        with open(lessonFilename) as lessonFile:
            entryFile = EntryFile(lessonFile)
            for entry in entryFile:
                note = self.col.newNote()
                note['Deutsch'] = entry.definition
                note[self.language] = entry.word
                note["Level"] = str(level)
                note.addTag("Level{:02}".format(level))
                if not entry.nr is None:
                    audioSrcFile = os.path.join(self.audioSrcPath, "{:02}{:02}.mp3".format(level, entry.nr))
                    if os.path.isfile(audioSrcFile):
                        audioDstFile = self.anki.copyToMediaDir(audioSrcFile, prefix="{}Aktiv".format(self.language))
                        note["Audio"] = "[sound:{}]".format(os.path.basename(audioDstFile))
                self.col.addNote(note)

    def importDirectory(self, path):
        for lessonDirname in sorted(glob.glob(os.path.join(path, "lektionen", "lek[0-9]*"))):
            match = match = re.match("lek([0-9]+)", os.path.basename(lessonDirname))
            level = int(match.group(1))
            lessonFilename = os.path.join(lessonDirname, "vok{}.txt".format(match.group(1)))
            if os.path.isfile(lessonFilename):
                self.importFile(lessonFilename, level)

    def run(self, path):
        self.path = self.findSubdirectory(os.path.abspath(path))
        if not self.path:
            raise ImporterException("The chosen directory does not contain Norwegisch/Schwedisch Aktiv!")
        self.audioSrcPath = os.path.join(self.path, "lekvok")
        
        self.language = self.findLanguage(self.path)
        if not self.language:
            raise ImporterException("Language could not be detected!")
        
        model = self.anki.createModel('{} Aktiv'.format(self.language), ['Deutsch', self.language, 'Audio', 'Level'])
        self.anki.addDefaultAudioFieldDecorator('Audio')
        self.anki.addTemplate(model, self.anki.createTemplate(u"{} -> {}".format('Deutsch', self.language), ['Deutsch'], [self.language, 'Audio']))
        self.anki.addTemplate(model, self.anki.createTemplate(u"{} -> {}".format(self.language, 'Deutsch'), [self.language, 'Audio'], ['Deutsch']))
        self.anki.selectModel(model)
        deck = self.anki.createDeck('{} Aktiv'.format(self.language))
        self.anki.selectDeck(deck)
        self.anki.saveDeckModelRelation(deck, model)
        
        self.importDirectory(self.path)

def startImport():
    d = QFileDialog(mw)
    d.setWindowTitle("Select the location of the CD")
    d.setFileMode(QFileDialog.Directory)
    d.setOption(QFileDialog.ShowDirsOnly, True)
    if d.exec_() == QDialog.Accepted:
        dirs= d.selectedFiles()
        if len(dirs):
            try:
                importer = Importer(mw.col)
                importer.run(dirs[0])
            
                mw.col.reset()
                mw.reset()
                # refresh deck browser so user can see the newly imported deck
                mw.deckBrowser.refresh()
            except ImporterException as e:
                QMessageBox.warning(mw, "Error", str(e))
            

action = QAction("Import Norwegisch/Schwedisch Aktiv...", mw)
mw.connect(action, SIGNAL("triggered()"), startImport)
mw.form.menuTools.addAction(action)

